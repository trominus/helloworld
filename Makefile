# Change your compiler here if you're not using gcc. CC is for C, and CXX is for C++.
CC = gcc
CXX = g++

# These are options that we pass to the compiler. They say to emit all warnings ("warn all"), and
# optimize the program aggresively to make it fast ("optimization level 3").
CFLAGS = -Wall -O3

# This line is here so that by default, Make will build both programs. It's not strictly necessary.
# With or without this line, you can run "make hello_world_c" or "make hello_world_cpp". With this
# line, if you just run "make" you'll get both. (Make executes the first rule in the Makefile when
# you don't give it a target specifically, so we put this rule first.) We tell Make that this rule
# is "phony" so that it understands it's just a combination of other rules and doesn't produce any
# of its own files.
.PHONY: all
all: hello_world_c hello_world_cpp

# These are essentially recipes for programs. The first line tells Make that we are going to build
# a program "hello_world_c" from the source file "hello_world.c". The second lines Make how to
# do it; Make basically copies and pastes this line into the terminal. Note that to use the compiler
# and options we defined earlier (CC, CXX, and CFLAGS), we have to enclose them in parentheses and
# add a dollar sign.
hello_world_c: hello_world.c
	$(CC) $(CFLAGS) -o hello_world_c hello_world.c

# Here's the same thing except for the C++ version.
hello_world_cpp: hello_world.cpp
	$(CXX) $(CFLAGS) -o hello_world_cpp hello_world.cpp

# This rule deletes all the binaries. It's declared "phony" which means it doesn't actually make
# anything. Otherwise Make would get confused about when it should run this rule.
.PHONY: clean
clean:
	rm -f hello_world_c hello_world_cpp
